'use strict'

module.exports = {
  siteMetadata: {
    title: 'som-bhaduri',
    description: 'A human being',
    siteUrl: 'https://somb.gitlab.io',
    author: {
      name: 'Som Bhaduri',
      url: 'https://somb.gitlab.io',
      email: 'sashmit@gmail.com'
    }
  },
  pathPrefix: `/`,
  plugins: [
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'content',
        path: `${__dirname}/src/content`
      }
    }, {
      resolve: 'gatsby-transformer-remark',
      options: {
        plugins: [
          {
            resolve: 'gatsby-remark-responsive-iframe',
            options: {
              wrapperStyle: 'margin-bottom: 1rem'
            }
          },
          'gatsby-remark-prismjs',
          'gatsby-remark-copy-linked-files',
          'gatsby-remark-smartypants', {
            resolve: 'gatsby-remark-images',
            options: {
              maxWidth: 1140,
              quality: 90,
              linkImagesToOriginal: false
            }
          }
        ]
      }
    },
    'gatsby-transformer-json', {
      resolve: 'gatsby-plugin-canonical-urls',
      options: {
        siteUrl: 'https://somb.gitlab.io'
      }
    },
    'gatsby-plugin-emotion',
    'gatsby-plugin-typescript',
    'gatsby-plugin-sharp',
    'gatsby-transformer-sharp',
    'gatsby-plugin-react-helmet'
  ]
}
